;;; Schema.lisp
;;; modified: 7 March 2021
;;; modified: 14 June 2024 - Fixed bug in handling propositions in input. Handle TRUE and FALSE.
;;; modified: 23 Sept 2024 - More robust handling of sat solver solution files
;;; modified: 25 Nov 2024 - Cleaned up, changed default back to compact-encoding.

(ql:quickload :cl-ppcre :silent t)

;; SAT program used by satisfy
(defvar sat-solver "kissat")

;; Muffle warnings about common lisp style
(declaim (sb-ext:muffle-conditions cl:style-warning))

;; Default values of options
(defvar compact-encoding t)
(defvar binary-functions '(eq neq = > < >= <= member
                              union intersection set-difference + - * div rem mod ** bit
                              range))
(defvar interpreted-functions (append '(not and or lisp set alldiff) binary-functions))
(defvar logical-connectives '(not and or implies if equiv all exists))
(defvar reserved-words (append interpreted-functions logical-connectives))

;; General string manipulation

(defun replace-suffix-with-regex (string pattern replacement)
  "Replace the PATTERN at the end of STRING with REPLACEMENT.
  PATTERN should be a regex matching the suffix."
  (if (cl-ppcre:scan pattern string :end (length string))
      (cl-ppcre:regex-replace pattern string replacement)
      string))

;;
;; File-based API: instantiate, propositionalize, interpret, satisfy
;; 

(defun satisfy (INFILE OUTFILE)
  (uiop:run-program sat-solver :arguments (list INFILE) :search-path t :output OUTFILE))

(defun instantiate (INFILE &optional OUTFILE OBSFILE)
  (if (null (cl-ppcre:scan "\\.."   INFILE))
    (setq INFILE (concatenate 'string INFILE ".wff")))
  (if (not OUTFILE)
      (setq OUTFILE (replace-suffix-with-regex INFILE "\\.wff$" ".scnf")))
  (with-open-file (INS INFILE :direction :input)
    (with-open-stream (OBS (if OBSFILE (open OBSFILE :direction :input) (make-concatenated-stream)))
      (with-open-file (OUTS OUTFILE :direction :output :if-exists :supersede)
        (let (CL SCHEMA OBSERVATION)
          (setq CL (parse
                     (loop while (not (eql 'EOF (setq SCHEMA (read INS nil 'EOF))))
                           collect SCHEMA)
                     (loop while (not (eql 'EOF (setq OBSERVATION (read OBS nil 'EOF))))
                           collect OBSERVATION)))
          (loop for C in CL do (format OUTS "~S~%" C)))))))

(defun propositionalize (WFFFILE &optional CNFFILE MAPFILE)
  (if (not CNFFILE)
      (setq CNFFILE (replace-suffix-with-regex WFFFILE "\\.scnf$" ".cnf")))
  (if (not MAPFILE)
      (setq MAPFILE (replace-suffix-with-regex WFFFILE "\\.scnf$" ".map")))
  (with-open-file (WS WFFFILE :direction :input)
    (with-open-file (CS CNFFILE :direction :output :if-exists :supersede)
      (with-open-file (MS MAPFILE :direction :output :if-exists :supersede)
        (multiple-value-bind (cnfdata mapdata numvar numclauses)
            (lit2prop (loop with clause while (not (eql :EOF (setq clause (read WS nil :EOF))))
                            collect clause))
          (format CS "p cnf ~S ~S~%" numvar numclauses)
          (loop for c in cnfdata do (format CS "~{~D ~}0~%" c))
          (format MS "map ~S~%" numvar)
          (loop for m in mapdata do (format MS "~{~D ~S~}~%" m)))))))

(defun read-lines (filename)
  ;; Read a text file and return a list of its lines
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil nil)
          while line
          collect line)))


(defun interpret (SOLNFILE &optional MAPFILE OUTFILE SORT_BY_LAST_ARGUMENT)
  (if (not MAPFILE)
      (setq MAPFILE (replace-suffix-with-regex SOLNFILE "\\.out$" ".map")))
  (if (not OUTFILE)
      (setq OUTFILE (replace-suffix-with-regex SOLNFILE "\\.out$" ".lits")))
  (let (solndata mapdata litdata)
    ;; Read solnfile to create solution list.  Ignore any non-integers and negative integers in solnfile.
    ;; Get list of lines of the solution
    (setq solndata (read-lines SOLNFILE))
    ;; On lines that begin with v, drop the v
    (setq solndata (mapcar (lambda (s) (if (cl-ppcre:scan "^v" s) (subseq s 1) s)) solndata))
    ;; Eliminate lines containing anything other than integers
    (setq solndata (remove-if (lambda (str) (cl-ppcre:scan "[^0-9\\s-]" str)) solndata))
    ;; Convert to a single string
    (setq solndata (format nil "~{~a~^ ~}" solndata))
    ;; Convert to a list of integers
    (setq solndata (mapcar #'parse-integer (ppcre:all-matches-as-strings "-?\\d+" solndata)))
    ;; Remove negativchee integers
    (setq solndata (remove-if (lambda (x) (<= x 0)) solndata))
    ;; Read mapfile to create mapdata list
    (with-open-file
        (MS MAPFILE :direction :input)
      (if (not (eql (read MS) 'map)) (error "Bad map file"))
      (if (not (integerp (read MS))) (error "Bad map file"))
      (setq mapdata (loop with i with p
                          while (not (eql :EOF (setq i (read MS nil :EOF))))
                          do (setq p (read MS)) (if (not (integerp i)) (error "Bad map file"))
                          collect (list i p))))
    ;; call soln2lit to create sorted list of true literals
    (setq litdata (soln2lit mapdata solndata sort_by_last_argument))
    ;; Print list of true literals to outfile
    (with-open-file
        (OS OUTFILE :direction :output :if-exists :supersede)
      (format OS "~{~S~%~}" litdata))))


;;
;; Subroutines for file-based API
;;

(defun lit2prop (CL)
  (let ((cnfdata nil) (mapdata nil) (numvar 0) (numclauses (length CL)) (hash (make-hash-table :test #'equal)))
    ;; Build hash table
    (loop for clause in CL do
            (loop for lit in (cdr clause) do
                    (let ((prop (if (is-proposition lit) lit (cadr lit))))
                      (cond ((not (nth-value 1 (gethash prop hash)))
                              (incf numvar)
                              (setf (gethash prop hash) numvar))))))
    ;; Translate clauses
    (setq cnfdata (loop for clause in CL
                        collect
                          (loop for lit in (cdr clause)
                                collect (if (is-proposition lit)
                                            (gethash lit hash)
                                            (- (gethash (cadr lit) hash))))))
    ;; Build map table
    (maphash #'(lambda (key val) (push (list val key) mapdata)) hash)
    (setq mapdata (sort mapdata #'< :key #'car))
    ;; Return multiple values
    (values cnfdata mapdata numvar numclauses)))


(defun step-ordering (p q)
  (step-order-r (if (atom p) p (reverse p))
                (if (atom q) q (reverse q))))

(defun step-order-r (p q)
  (cond ((and (integerp p) (integerp q)) (< p q))
        ((and (atom p) (atom q)) (string-lessp p q))
        ((atom p) t)
        ((atom q) nil)
        ((equal (car p) (car q)) (prop-order-r (cdr p) (cdr q)))
        (t (step-order-r (car p) (car q)))))


;;
;; Internal API: parse
;; 

(defun setup-global-env ()
  ; Set up global environment
  (declare (special Bind ObservedPredicates ObservedLiterals))
  (setq Bind (make-hash-table :test #'eql))
  (setq ObservedPredicates (make-hash-table :test #'eql))
  (setq ObservedLiterals (make-hash-table :test #'equal))
  (setf (gethash 'TRUE ObservedPredicates) 1)
  (setf (gethash 'TRUE ObservedLiterals) 1)
  (setf (gethash 'FALSE ObservedPredicates) 1)
  (setf (gethash 'FALSE ObservedLiterals) 0))

(defun parse (SCHEMA-LIST &optional OBSERVATION-LIST)
  (let (Bind ObservedPredicates ObservedLiterals)
    (setup-global-env)
    (parse-observations OBSERVATION-LIST)
    (mapcar #'(lambda (c) (cons 'or c))
      (remove-valid-clauses
        (parse-schema-list SCHEMA-LIST)))))


(defun parse-observations (OBSERVATION-LIST)
  (declare (special ObservedPredicates ObservedLiterals))
  (cond ((null OBSERVATION-LIST) nil)
        (t
          ;; handle propositions as well as zero-ary literals
          (setf (gethash
                  (if (listp (car OBSERVATION-LIST))
                      (caar OBSERVATION-LIST)
                      (car OBSERVATION-LIST))
                  ObservedPredicates)
            1)
          (setf (gethash (car OBSERVATION-LIST) ObservedLiterals) 1)
          (parse-observations (cdr OBSERVATION-LIST)))))

;
; Recursive version blew up recursion stack
;
; (defun remove-valid-clauses (CL)
;  (cond ((null CL) nil)
;	((valid (car CL)) (remove-valid-clauses (cdr CL)))
;	(t (cons (car CL) (remove-valid-clauses (cdr CL))))))
;
(defun remove-valid-clauses (CL)
  (let (answer)
    (dolist (c CL)
      (if (null (valid c))
          (setq answer (cons c answer))))
    answer))

(defun valid (C)
  (cond ((null C) nil)
        ((member (complement-literal (car C)) (cdr C) :test #'equal) t)
        (t (valid (cdr C)))))

(defun complement-literal (L)
  (cond ((atom L) (list 'not L))
        ((eql (car L) 'not) (cadr L))
        (t (list 'not L))))

(defun parse-schema-list (SCHEMA-LIST)
  (cond ((null SCHEMA-LIST) nil)
        (t (append (parse-schema (car SCHEMA-LIST))
             (parse-schema-list (cdr SCHEMA-LIST))))))

(defun parse-schema (SCHEMA)
  (cond ((atom SCHEMA) (parse-formula SCHEMA))
        ((eql (car SCHEMA) 'type) (parse-type (cdr SCHEMA)))
        ((eql (car SCHEMA) 'constant) (parse-type (cdr SCHEMA)))
        ((eql (car SCHEMA) 'option) (parse-option (cdr SCHEMA)))
        (t (parse-formula SCHEMA))))

(defun parse-option (ARGS)
  (let ((opt (car ARGS))
        (val (parse-expression (cadr ARGS))))
    (if (eql val 0) (setq val nil))
    (cond ((member opt '(compact-encoding))
            (set opt val))
          (t (error "Cannot parse option ~S" ARGS)))
    nil))

(defun parse-type (DEFINITION)
  (bind-uniquely (car DEFINITION)
                 (parse-expression (cadr DEFINITION)) t)
  nil)

(defun bind-uniquely (VAR VAL &optional noerror)
  (declare (special Bind))
  (if (gethash VAR Bind)
      (if noerror
          nil
          (error "Cannot shadow variable ~S with binding ~S" VAR VAL))
      (setf (gethash VAR Bind) VAL)))

(defun unbind (VAR)
  (declare (special Bind))
  (remhash VAR Bind))

(defun is-bound (VAR)
  (declare (special Bind))
  (gethash VAR Bind))

(defun binding-of (VAR)
  (declare (special Bind))
  (gethash VAR Bind))

(defun is-observed-literal (F)
  (declare (special ObservedPredicates))
  (cond ((not (is-literal F)) nil)
        ((and (listp F) (eql (car F) 'not))
          (is-observed-literal (cadr F)))
        ((listp F)
          (gethash (car F) ObservedPredicates))
        (t
          (gethash F ObservedPredicates))))

;; (defun is-observed-literal (F)
;;   (declare (special ObservedPredicates))
;;   (and (is-literal F)
;;        ;; (listp F)
;;        ;; make work for propositions as well as zero-ary predicates
;;        (if (eql (car F) 'not)
;; 	   (is-observed-literal (cadr F))
;; 	   (gethash
;; 	    (if (listp F) (car F) F)
;; 	    ObservedPredicates))))

(defun parse-observed-literal (F)
  ;; returns NIL if observed literal is true and (nil) if it is false
  (declare (special ObservedLiterals))
  (let ((answ (cond ((and (listp F) (eql (car F) 'not))
                      (if (is-true (gethash (parse-literal (cadr F)) ObservedLiterals 0))
                          '(())
                          '()))
                    (t
                      (if (is-true (gethash (parse-literal F) ObservedLiterals 0))
                          '()
                          '(()))))))
    answ))


(defun parse-formula (F)
  ;; (format t "entering parse ~S" F)
  (cond ((is-observed-literal F) (parse-observed-literal F))
        ((is-literal F) (list (list (parse-literal F))))
        ((eql (car F) 'not) (parse-not (cadr F)))
        ((eql (car F) 'and) (parse-and (cdr F)))
        ((eql (car F) 'or) (parse-or (cdr F)))
        ((eql (car F) 'implies) (parse-implies (cdr F)))
        ((eql (car F) 'if) (parse-if (cadr F) (caddr F)))
        ((eql (car F) 'equiv) (parse-equiv (cdr F)))
        ((eql (car F) 'all)
          (parse-all (cadr F)
                     (parse-set-expression (caddr F))
                     (cadddr F)
                     (car (cddddr F))))
        ((eql (car F) 'exists)
          (parse-exists (cadr F)
                        (parse-set-expression (caddr F))
                        (cadddr F)
                        (car (cddddr F))))
        (t (error "Cannot parse formula ~S" F))))

(defun parse-if (test body)
  (cond ((is-false (parse-integer-expression test)) nil)
        (t (parse-formula body))))

(defun parse-not (F)
  ;; F is not a literal, that case is handled in parse-formula
  ;; (format t "entering parse-not ~S" F)
  (let ((op (car F)))
    (cond ((eql op 'not) (parse-formula (cadr F)))
          ((eql op 'and) (parse-formula (cons 'or (negate-list (cdr F)))))
          ((eql op 'or) (parse-formula (cons 'and (negate-list (cdr F)))))
          ((eql op 'implies) (parse-formula (list 'and (cadr F) (list 'not (caddr F)))))
          ((eql op 'if) (cond ((is-true (parse-integer-expression (cadr F))) (parse-formula `(not ,(caddr F))))
                              (t '(()))))
          ((eql op 'equiv) (append (parse-formula `(or ,(cadr F) ,(caddr F)))
                             (parse-formula `(or (not ,(cadr F)) (not ,(caddr F))))))
          ((eql op 'all) (parse-formula `(exists ,(cadr F) ,(caddr F) ,(cadddr F) (not ,(car (cddddr F))))))
          ((eql op 'exists) (parse-formula `(all ,(cadr F) ,(caddr F) ,(cadddr F) (not ,(car (cddddr F))))))
          (t (error "Cannot parse negation ~S" F)))))

(defun negate-list (L)
  (cond ((null L) nil)
        (t (cons (list 'not (car L)) (negate-list (cdr L))))))

(defun parse-implies (FL)
  (if (not (= (length FL) 2)) (error "Cannot parse implication ~S" FL))
  (parse-or (cons (list 'not (car FL)) (cdr FL))))

(defun parse-equiv (FL)
  (if (not (= (length FL) 2)) (error "Cannot parse equivalence ~S" FL))
  (append (parse-implies FL)
    (parse-formula `(implies ,(cadr FL) ,(car FL)))))

(defun is-literal (F)
  (or (is-proposition F)
      (and (eql 'not (car F)) (is-proposition (cadr F)))))

(defun is-proposition (F)
  (or (atom F)
      (not (member (car F) logical-connectives))))

(defun parse-and (FL) ;; and just appends the clauses
  (parse-schema-list FL))

(defun parse-or (FL)
  (cond ((null FL) (list nil)) ;; empty OR is the empty clause
        (t (multiply-clauses (parse-schema (car FL))
                             (parse-or (cdr FL))))))

(defun multiply-clauses (L R)
  (if (or (null compact-encoding)
          (< (length L) 2)
          (< (length R) 2)
          (< (+ (length L) (length R)) 5))
      (explicit-multiply-clauses L R)
      (let ((g (gensym "XX"))) ;; g selects whether L or R must be true
        (append (mapcar #'(lambda (c) (cons g c)) R)
          (mapcar #'(lambda (c) (cons (list 'not g) c)) L)))))

(defun merge-clauses (C1 C2)
  (remove-duplicates (append C1 C2) :test #'equal))

; Reduce stack requirements - rewrite following two recursive functions
;  as a single iterative function.
;
;(defun explicit-multiply-clauses (L R)
;  (cond ((null L) nil)
;	(t (append (multiply-one-clause (car L) R)
;		   (explicit-multiply-clauses (cdr L) R)))))
;
;(defun multiply-one-clause (C R)
;  (cond ((null R) nil)
;	(t (append (list (merge-clauses C (car R)))
;		   (multiply-one-clause C (cdr R))))))


(defun explicit-multiply-clauses (L R)
  (let (answer)
    (dolist (lclause L)
      (setq answer (cons (multiply-one-clause lclause R) answer)))
    (mapcan #'copy-list answer)))

(defun multiply-one-clause (C R)
  (let (answer)
    (dolist (rclause R)
      (setq answer (cons (merge-clauses C rclause) answer)))
    answer))

;; To do: in the case where a list of variables is specified,
;; also assume a list of domains is specified

(defun parse-all (VAR DOM TEST BODY)
  (cond ((null DOM) nil) ;; the empty list of clauses
        ;; a single variable is specified
        ((not (listp VAR)) (append (parse-binding VAR (car DOM) TEST BODY nil)
                             (parse-all VAR (cdr DOM) TEST BODY)))
        ;; a list of variables is specified
        (t (parse-formula (expand-multivar-all VAR DOM TEST BODY)))))


(defun parse-exists (VAR DOM TEST BODY)
  (cond ((NULL Dom) (list nil)) ;; the empty clause
        ;; a single variable is specified
        ((not (listp VAR)) (multiply-clauses (parse-binding VAR (car DOM) TEST BODY (list nil))
                                             (parse-exists VAR (cdr DOM) TEST BODY)))
        ;; a list of variables is specified
        (t (parse-formula (expand-multivar-exists VAR DOM TEST BODY)))))


;; Is failing for complex terms - why?
;; try using simple terms

(defun parse-for (VAR DOM TEST BODY)
  (cond ((null DOM) nil) ;; the empty list of clauses
        ;; a single variable is specified
        ((not (listp VAR)) (append (parse-expression-binding VAR (car DOM) TEST BODY nil)
                             (parse-for VAR (cdr DOM) TEST BODY)))
        ;; a list of variables is specified
        (t (parse-formula (expand-multivar-for VAR DOM TEST BODY)))))

(defun parse-expression-binding (VAR VAL TEST BODY FAILED-TEST-RESULT)
  (declare (special Bind))
  (let ((RESULT FAILED-TEST-RESULT))
    (bind-uniquely VAR VAL)
    (let ((TESTVAL (parse-expression TEST)))
      (if (is-true TESTVAL)
          (setq RESULT (parse-expression BODY))))
    (unbind VAR)
    RESULT))

(defun expand-multivar-for (VARLIST DOM TEST BODY)
  (cond ((null VARLIST) BODY)
        ((null (cdr VARLIST))
          `(for ,(car VARLIST) (set ,@DOM) ,TEST ,BODY))
        (t
          `(for ,(car VARLIST) (set ,@DOM) t
                ,(expand-multivar-for (cdr VARLIST) DOM TEST BODY)))))


(defun expand-multivar-all (VARLIST DOM TEST BODY)
  (cond ((null VARLIST) BODY)
        ((null (cdr VARLIST))
          `(all ,(car VARLIST) (set ,@DOM) ,TEST ,BODY))
        (t
          `(all ,(car VARLIST) (set ,@DOM) t
                ,(expand-multivar-all (cdr VARLIST) DOM TEST BODY)))))


(defun expand-multivar-exists (VARLIST DOM TEST BODY)
  (cond ((null VARLIST) BODY)
        ((null (cdr VARLIST))
          `(exists ,(car VARLIST) (set ,@DOM) ,TEST ,BODY))
        (t
          `(exists ,(car VARLIST) (set ,@DOM) t
                   ,(expand-multivar-exists (cdr VARLIST) DOM TEST BODY)))))

(defun is-false (x)
  (or (null x) (eql x 0)))

(defun is-true (x)
  (not (is-false x)))

(defun parse-binding (VAR VAL TEST BODY FAILED-TEST-RESULT)
  (declare (special Bind))
  (let ((RESULT FAILED-TEST-RESULT))
    (bind-uniquely VAR VAL)
    (let ((TESTVAL (parse-expression TEST)))
      (if (is-true TESTVAL)
          (setq RESULT (parse-schema BODY))))
    (unbind VAR)
    RESULT))

(defun parse-set-expression (EXPR)
  (let ((answ (parse-expression EXPR)))
    (if (not (listp answ)) (error "Set expected instead of ~S" EXPR))
    answ))

(defun parse-integer-expression (EXPR)
  (let ((answ (parse-expression EXPR)))
    (if (not (integerp answ)) (error "Integer expected instead of ~S" EXPR))
    answ))

(defun parse-name (EXPR)
  (if (or (null EXPR)
          (integerp EXPR)
          (listp EXPR)
          (member EXPR reserved-words))
      (error "Symbol expected instead of ~S" EXPR))
  EXPR)

(defun parse-or-expression (EXPR)
  (cond ((null EXPR) 0)
        ((is-true (parse-expression (car EXPR))) 1)
        (t (parse-or-expression (cdr EXPR)))))

(defun parse-and-expression (EXPR)
  (cond ((null EXPR) 1)
        ((is-false (parse-expression (car EXPR))) 0)
        (t (parse-and-expression (cdr EXPR)))))

(defun evaluate-lisp-expression (EXPR)
  (declare (special Bind))
  (maphash #'set Bind)
  (eval EXPR))

(defun all-different (symbols)
  (cond ((null symbols) t)
        ((null (cdr symbols)) t)
        (t (and (not (member (car symbols) (cdr symbols)))
                (all-different (cdr symbols))))))

(defun parse-expression (EXPR)
  (declare (special Bind ObservedPredicates))
  (cond ((and (symbolp EXPR) (not (null EXPR)) (is-bound EXPR)) (binding-of EXPR))
        ((atom EXPR) EXPR)
        (t (let ((op (car EXPR)))
             (cond ((eql op 'not) (if (is-true (parse-expression (cadr EXPR))) 0 1))
                   ((eql op 'and) (parse-and-expression (cdr EXPR)))
                   ((eql op 'or) (parse-or-expression (cdr EXPR)))
                   ((eql op 'set) (parse-enumerated-set (cdr EXPR)))
                   ((eql op 'for) (parse-for (cadr EXPR) (parse-set-expression (caddr EXPR))
                                             (cadddr EXPR) (car (cddddr EXPR))))
                   ((eql op 'alldiff) (all-different (mapcar #'parse-expression (cdr EXPR))))
                   ((gethash op ObservedPredicates)
                     (parse-observed-literal-expression EXPR))
                   ((eql op 'lisp)
                     (evaluate-lisp-expression (cadr EXPR)))
                   ((and (member op binary-functions) (= (length EXPR) 3))
                     (parse-binary-expression op (cadr EXPR) (caddr EXPR)))
                   (t (error "Parser error at ~S" EXPR)))))))


(defun parse-binary-expression (op LEFT RIGHT)
  (let ((e1 (parse-expression LEFT)) (e2 (parse-expression RIGHT)))
    (cond ((eql op 'member) (if (member e1 e2) 1 0))
          ((or (eql op 'eq) (eql op '=)) (if (equalp E1 E2) 1 0))
          ((eql op 'neq) (if (equalp E1 E2) 0 1))
          ((eql op '<) (if (< e1 e2) 1 0))
          ((eql op '>) (if (> e1 e2) 1 0))
          ((eql op '<=) (if (<= e1 e2) 1 0))
          ((eql op '>=) (if (>= e1 e2) 1 0))
          ((eql op '+) (+ e1 e2))
          ((eql op '-) (- e1 e2))
          ((eql op '*) (* e1 e2))
          ((eql op 'bit) (logand 1 (lsh e2 (- 1 e1))))
          ((eql op '**) (expt e1 e2))
          ((eql op 'div) (floor (/ e1 e2)))
          ((eql op 'rem) (- e1 (* e2 (floor (/ e1 e2)))))
          ((eql op 'mod) (mod e1 e2))
          ((eql op 'range) (parse-range e1 e2))
          ((eql op 'union) (union e1 e2))
          ((eql op 'intersection) (intersection e1 e2))
          ((eql op 'set-difference) (set-difference e1 e2))
          (t (error "Parser error at ~S" op)))))


(defun parse-range (LOW HIGH)
  (cond ((> LOW HIGH) nil)
        (t (cons LOW (parse-range (+ 1 LOW) HIGH)))))

(defun parse-observed-literal-expression (EXPR)
  (declare (special ObservedLiterals))
  (gethash (cons (car EXPR) (map 'list #'parse-expression (cdr EXPR)))
           ObservedLiterals 0))

(defun parse-enumerated-set (EXPR)
  (cond ((null EXPR) nil)
        (t (cons (parse-term (car EXPR))
                 (parse-enumerated-set (cdr EXPR))))))

(defun parse-literal (LIT)
  (cond ((and (listp LIT) (eq (car LIT) 'not))
          (list 'not (parse-proposition (cadr LIT))))
        (t (parse-proposition LIT))))

(defun parse-proposition (P)
  (cond ((null P) (error "Unexpected empty set"))
        ((atom P) P)
        (t (cons (parse-name (car P)) (parse-terms (cdr P))))))

(defun parse-terms (TERMS)
  (cond ((null TERMS) nil)
        (t (cons (parse-term (car TERMS)) (parse-terms (cdr TERMS))))))

(defun parse-term (TERM)
  (cond ((or (atom TERM) (member (car TERM) interpreted-functions))
          (parse-expression TERM))
        (t (cons (parse-name (car TERM))
                 (parse-terms (cdr TERM))))))


(defun soln2lit (mapdata solndata &optional sort-by-time)
  ;; return a list of propositions
  (let ((hash (make-hash-table)) proplist)
    (loop for pair in mapdata do (setf (gethash (car pair) hash) (cadr pair)))
    (setq proplist (loop for i in solndata collect (gethash i hash)))
    (setq proplist (sort proplist (if sort-by-time #'time-ordering #'alpha-ordering)))
    proplist))

(defun alpha-ordering (p q)
  (string-lessp (format nil "~s" p) (format nil "~s" p)))


(defun time-order-r (p q)
  (cond ((and (integerp p) (integerp q)) (< p q))
        ((and (atom p) (atom q)) (string-lessp p q))
        ((atom p) t)
        ((atom q) nil)
        ((equal (car p) (car q)) (prop-order-r (cdr p) (cdr q)))
        (t (time-order-r (car p) (car q)))))

(defun time-ordering (p q)
  (time-order-r (if (atom p) p (reverse p))
                (if (atom q) q (reverse q))))
